# Arvato Automation Test Framework
Automation Framework for running end2end UI tests

## Framework Used
[Cypress](https://www.cypress.io/)
[Cucumber](https://cucumber.io/)

## How to Run test ?
Pre requisite : `node 16+` version should be installed in your machine

Clone the repo
Open the command window and run the following commands after changing directory to inside the repo
```
npm install
npx cypress install
npm run execute:tests
```

The tests will be run in chrome browser and the results will be opened after the execution

### TODO
- [ ] Implement screenshot in cucumber report
- [ ] Parameterize the browser
- [ ] Run the tests in docker
- [ ] Add CI pipeline and MR process for merging to master
