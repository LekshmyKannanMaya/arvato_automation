/// <reference types="cypress" />
// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

const cucumber = require("cypress-cucumber-preprocessor").default

// eslint-disable-next-line no-unused-vars
module.exports = (on, config) => {
	on("file:preprocessor", cucumber())
	on("after:run", (results) => {
		var fs = require("fs")
		var dir = "cypress/cucumber-reports"

		if (!fs.existsSync(dir)){
			fs.mkdirSync(dir)
		}
		const reporter = require("cucumber-html-reporter")
		const options = {
			theme: "hierarchy",
			jsonDir: "cypress/cucumber-json",
			output: "cypress/cucumber-reports/result.html",
			reportSuiteAsScenarios: true,
			launchReport: true,
			ignoreBadJsonFile: true,
			scenarioTimestamp: true,
			metadata: {
				Browser: results.browserName,
				"Browser Version": results.browserVersion,
				"Total Tests": results.totalTests,
				"Passed Tests": results.totalPassed,
				"Failed Tests": results.totalFailed,
				"Pending Tests": results.totalPending,
				"Skipped Tests": results.totalSkipped
			}
		}
		reporter.generate(options)
	})
}
