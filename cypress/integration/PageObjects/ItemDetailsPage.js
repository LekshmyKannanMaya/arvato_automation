class ItemDetailsPage {
	addToCartButton () {
		return cy.get("#add-to-cart-button")
	}

	addToCart () {
		this.addToCartButton().click()
	}
}
export default ItemDetailsPage
