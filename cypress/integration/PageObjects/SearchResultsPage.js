class SearchResultsPage {
	sortListItem () {
		return cy.get("select[id=\"s-result-sort-select\"]")
	}

	firstSearchItem () {
		return cy.get("[data-component-type=\"s-search-result\"] .a-price").first()
	}

	sortByPriceAscending () {
		this.sortListItem().select("price-asc-rank", { force: true })
	}

	openCheapestItem () {
		this.firstSearchItem().click()
	}
}
export default SearchResultsPage
