class HomePage {
	cookies () {
		return cy.get("[id=sp-cc-accept]:visible")
	}

	searchBox () {
		return cy.get("#twotabsearchtextbox")
	}

	searchButton () {
		return cy.get("#nav-search-submit-button")
	}

	searchItem (itemName) {
		this.searchBox().type(itemName)
		this.searchButton().click({ force: true })
	}
}
export default HomePage
