class CartPage {
	checkOutButton () {
		return cy.get("input[name=\"proceedToRetailCheckout\"]")
	}

	cartButton () {
		return cy.get("#nav-cart-count")
	}

	priceofItem () {
		return cy.get("[data-name=\"Active Items\"] .sc-product-price")
	}

	totalPrice () {
		return cy.get("[data-name=\"Subtotals\"] #sc-subtotal-amount-activecart .sc-price")
	}

	sumAmount () {
		return cy.get("[class=\"a-cardui-body\"] .sc-price")
	}

	clickcheckOutCart () {
		this.checkOutButton().click()
	}

	clickCartButton () {
		this.cartButton().click()
	}

	getPriceandCompareSum () {
		const isEqual = this.priceofItem().eq(0).invoke("text")
			.then((txt) => {
				cy.parseLocaleNumber(txt).then(result => {
					const price1 = result
					this.priceofItem().eq(1).invoke("text").then(txt => {
						cy.parseLocaleNumber(txt).then(result => {
							const price2 = result
							this.totalPrice().invoke("text").then(txt => {
								cy.parseLocaleNumber(txt).then(result => {
									const totalPrice = result
									cy.log(totalPrice)
									if (totalPrice == (price1 + price2)) {
										return Promise.resolve(true)
									} else {
										return Promise.resolve(false)
									}
								})
							})
						})
					})
				})
			})
		return isEqual
	}
}
export default CartPage
