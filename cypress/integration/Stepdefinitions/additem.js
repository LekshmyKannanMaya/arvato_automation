/// <reference types="Cypress" />
import { Given, When, Then } from "cypress-cucumber-preprocessor/steps"
import HomePage from "../PageObjects/HomePage"
import SearchResultsPage from "../PageObjects/SearchResultsPage"
import ItemDetailsPage from "../PageObjects/ItemDetailsPage"
import RegisterPage from "../PageObjects/RegisterPage"
import CartPage from "../PageObjects/CartPage"

const homepage = new HomePage()
const searchResultsPage = new SearchResultsPage()
const itemDetailsPage = new ItemDetailsPage()
const registerPage = new RegisterPage()
const cartPage = new CartPage()

Given(/^I navigate to amazon page$/, () => {
	// TODO Read the url from env file
	cy.visit("https://www.amazon.de")
	homepage.cookies().click()
})

When(/^I search for "([^"]*)"$/, (itemName) => {
	homepage.searchItem(itemName)
})

Then(/^I filter to ascending order$/, () => {
	searchResultsPage.sortByPriceAscending()
})

When(/^I add the lowest priced "([^"]*)" to the cart$/, () => {
	searchResultsPage.openCheapestItem()
	itemDetailsPage.addToCart()
})

When(/^I check if its displayed in the cart$/, () => {
	cartPage.clickCartButton()
})

Then(/^I verify the sum amount$/, () => {
	cartPage.getPriceandCompareSum().then(result => {
		expect(result).to.eq(true)
	})
})

Then(/^I click Checkout button$/, () => {
	cartPage.clickcheckOutCart()
})

Then(/^I verify if user registration page is displayed$/, () => {
	registerPage.loginTextVerify()
})
