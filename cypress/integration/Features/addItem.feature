Feature: addItem

Scenario Outline: Add lowest priced Items from amazon.de and verify if the sum in cart is correct

    Given I navigate to amazon page 
    When I search for "<Item>"
    And I filter to ascending order
    When I add the lowest priced "<Item>" to the cart
    Then I search for "<Item1>"
    And I filter to ascending order
    When I add the lowest priced "<Item1>" to the cart
    And I check if its displayed in the cart
    Then I verify the sum amount
    When I click Checkout button
    Then I verify if user registration page is displayed

    Examples:
    |Item    |Item1|
    |Snickers|Skittles|

