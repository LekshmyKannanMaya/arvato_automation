Cypress.Commands.add("parseLocaleNumber", (stringNumber) => {
	const thousandSeparator = Intl.NumberFormat("de").format(11111).replace(/\p{Number}/gu, "")
	const decimalSeparator = Intl.NumberFormat("de").format(1.1).replace(/\p{Number}/gu, "")
	return parseFloat(stringNumber
		.replace(new RegExp("\\" + thousandSeparator, "g"), "")
		.replace(new RegExp("\\" + decimalSeparator), "."))
})
